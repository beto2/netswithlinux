# Pasos previos

Instalar git en la máquina:

    dnf -y install git

Y realizamos la configuracón inicial de git sustituyendo los datos
de ejemplo por datos reales:

    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git config --global push.default simple


Crear una cuenta en gitlab [http://gitlab.com]

Asociar una clave ssh a nuestra cuenta. Para eso si no se dispone de 
una clave hay un tutorial en [https://gitlab.com/help/ssh/README]

Y desde la sección profile de gitlab se añade una clave: [https://gitlab.com/profile/keys]

# Crear un fork del proyecto del profesor

Un fork es crear tu propio repositorio a partir de una copia de otro 
repositorio de git. Hay una explicación más detallada en [http://aprendegit.com/fork-de-repositorios-para-que-sirve/]http://aprendegit.com/fork-de-repositorios-para-que-sirve/]

Desde el navegador abrir sesión en gitlab con tu usuario e 
ir a la página de gitlab del proyecto del profesor, 
en nuestro caso: [https://gitlab.com/beto/netswithlinux]

Desde allí hacemos un click en el botón de fork que nos redirige a esta
url: [https://gitlab.com/beto/netswithlinux/forks/new]

Una vez creado el fork disponemos de nuestro repositorio al que podemos
acceder desde la url [https://gitlab.com/USUARIO_DE_GITLAB/netswithlinux]

Ahora hay que dar permisos al profesor para que pueda modificar tu repositorio, 
para ello hay que ir a [https://gitlab.com/USUARIO_DE_GITLAB/netswithlinux/project_members] 
y seleccionara al usuario tecleando: Alberto Larraz y seleccionando de la
lista al username beto tal y como se aprecia en esta captura de pantalla:
[https://gitlab.com/USUARIO_DE_GITLAB/netswithlinux/project_members]

# Trabajar con el repositorio en tu equipo

Trabajamos en el directorio ~/m07. Creamos el directorio y nos situamos
dentro:

    mkdir ~/m07
    cd ~/m07
    
Ahora clonamos nuestro repositorio:

    git@gitlab.com:USUARIO_DE_GITLAB/netswithlinux.git
    
Nos situamos dentro del repositorio:

    cd netswithlinux
    
y creamos una nueva carpeta works

    mkdir works
    
copiamos en enunciado del ejercicio que queremos contestar, por ejemplo:
el 01:
    cp exercises/01_*  works/
    
y añadimos esa carpeta a nuestro repositorio local

    git add  works/
    
Ahora hacemos alguna modificación en el fichero works/01_subnetting.md y 
cuando tengamos una versión del documento que queramos salvar hacemos un
commit.

    git commit -am "primeras modificaciones en ejercicio1"
    
Si queremos subir ese commit al repositorio de gitlab (upsttream) sólo 
hemos de hacer un git push sin parámetros, ya que estamos en la rama master

    git push
    
# Añadir cambios del repositorio principal

Si el profesor cuelga un nuevo ejercicio o un nuevo documento en el 
repositorio principal, estas novedades han de incorporarse en nuestro
propio repositorio. Para eso hemos de añadirnos el repositorio del
profesor como remote. 

Para ver los repositorios remotos que tenemos ejecutamos:

    $ git remote -v
    origin	git@gitlab.com:beto2/netswithlinux.git (fetch)
    origin	git@gitlab.com:beto2/netswithlinux.git (push)

Y nos responde con dos líneas. El repositorio **origin**, es el repositorio
del cual es propietario nuestra cuenta en gitlab. 

Llamaremos repositorio **upstream** al repositorio 
del profesor, del cual hemos hecho un fork.

Para añadir este repositorio remoteo ejecutamos:
    
    git remote add upstream https://gitlab.com/beto/netswithlinux.git
    
Ahora debería aparecer:

    $ git remote -v
    origin	git@gitlab.com:beto2/netswithlinux.git (fetch)
    origin	git@gitlab.com:beto2/netswithlinux.git (push)
    upstream	https://gitlab.com/beto/netswithlinux.git (fetch)
    upstream	https://gitlab.com/beto/netswithlinux.git (push)

Si ha habido alguna modificación en el repositorio upstream en la rema master y queremos
actualizar el nuestro hacemos:

    git fetch upstream master
    
Esto actualiza en nuestro equipo la rama upstresm/master. Si queremos 
hacer un merge con nuestra rama local de nuestro repositorio 
nos aseguramos que estamos trabajando en nuestra rama master y luego
hacemos un merge:

    git checkout master
    git merge upstream/master
    
Al hacer el merge se genera un nuevo commit, y nos pregunta el mensaje
de ese commit abriendo un editor, en nuestro casi vim. Escribimos algo
o lo dejemos tal como está y guardamos. 

Si queremos actualizar estos cambios en nuestro repositorio origin como
nuestra rama local master hace pull de la rama origin haciendo un 
git push subimos los commits locales a la rama master de origin

    git push
    


